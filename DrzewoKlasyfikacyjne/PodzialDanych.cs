﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrzewoKlasyfikacyjne
{
    /// <summary>
    /// Klasa ma na celu podzielenie danych na część służącą do budowy drzewa klasyfikacyjnego i część służącą do walidacji wcześniej zbudowanego drzewa.
    /// </summary>
    public class PodzialDanych
    {
        //Fields
        DataView wszystkieDane;
        int uczenie;
        int walidacja;

        //Properties
        /// <summary>
        /// Tabela ze wszystkimi danymi, które zostały dostarczone do podziału.
        /// </summary>
        public DataView WszystkieDane { get { return wszystkieDane; } }
        /// <summary>
        /// Procent danych uczących.
        /// </summary>
        public int Uczenie { get { return uczenie; } }
        /// <summary>
        /// Procent danych walidacyjnych.
        /// </summary>
        public int Walidacja { get { return walidacja; } }
        /// <summary>
        /// Dane przeznaczone do budowy drzewa klasyfikacyjnego.
        /// </summary>
        public DataView DaneUczenie { get; set; }
        /// <summary>
        /// Dane przeznaczone do oceny zbudowanego wcześniej drzewa klasyfikacyjnego.
        /// </summary>
        public DataView DaneWalidacja { get; set; }

        //Constructor
        /// <summary>
        /// Tworzy nowy obiekt, który dzieli dane na dwa podzbiory.
        /// </summary>
        /// <param name="dane"></param>
        /// <param name="daneUczace">Procent danych uczących.</param>
        /// <param name="walidacja">Procent danych walidacyjnych.</param>
        public PodzialDanych(DataView dane, int uczenie, int walidacja)
        {
            if (uczenie + walidacja != 100)
            {
                throw new ArgumentException("Dane uczące w sumie z danymi walidacji muszą dać razem 100%.");
            }

            this.wszystkieDane = dane;
            this.uczenie = uczenie;
            this.walidacja = walidacja;
            this.DaneUczenie = dane.StworzPustaTabeleZTymiSamymiKolumnami();
            this.DaneWalidacja = dane.StworzPustaTabeleZTymiSamymiKolumnami();

            PodzielDane();
        }

        //Methods
        /// <summary>
        /// Metoda dzieli dane na 2 grupy, czyli DaneUczenie i DaneWalidacja.
        /// </summary>
        private void PodzielDane()
        {
            int liczbaRekordow = this.wszystkieDane.Table.Rows.Count;
            double procentUczenie = Convert.ToDouble(uczenie) / 100.0;

            int rowNr = 0;
            foreach (DataRow row in this.wszystkieDane.Table.Rows)
            {
                double procentPrzejzanychDanych = (double)rowNr / liczbaRekordow;
                if (procentPrzejzanychDanych <= procentUczenie)
                {
                    this.DaneUczenie.Table.ImportRow(row);
                }
                else
                {
                    this.DaneWalidacja.Table.ImportRow(row);
                }

                rowNr++;
            }
        }
    }
}
