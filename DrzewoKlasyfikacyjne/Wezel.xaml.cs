﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DrzewoKlasyfikacyjne
{
    /// <summary>
    /// Interaction logic for Wezel.xaml
    /// </summary>
    public partial class Wezel : UserControl
    {
        //Fields
        private PodsumowanieTabeli podsumowanieTabeli;

        //Properties
        public string NazwaKolumny { get { return this.NazwaTabeli.Text; } set { this.NazwaTabeli.Text = value; } }
        public string NazwaWartosciKlasyfikacji
        {
            get { return this.WartoscKlasyfikacji.Text.Substring(1, this.WartoscKlasyfikacji.Text.Length - 2); }
            set
            {
                if (string.IsNullOrWhiteSpace(value)) this.WartoscKlasyfikacji.Text = "  ";
                else this.WartoscKlasyfikacji.Text = string.Format("[{0}]", value);
            }
        }

        //Constructors
        public Wezel()
        {
            InitializeComponent();
        }

        public Wezel(PodsumowanieTabeli podsumowanieTabeli)
        {
            this.InitializeComponent();
            this.podsumowanieTabeli = podsumowanieTabeli;

            this.NazwaTabeli.Text = this.podsumowanieTabeli?.KolumnaZNajnizszaEntropia?.NazwaKolumny;
            this.LiczbaRekordowPrzypisanychPoprawnie.Text = "True Positive: " + this.podsumowanieTabeli?.ZbiorZatwierdzony?.Table.Rows.Count.ToString();
            this.WalidacjaPotwierdzoneDataGrid.ItemsSource = this.podsumowanieTabeli?.ZbiorZatwierdzony;
        }

        //Methods
    }
}
