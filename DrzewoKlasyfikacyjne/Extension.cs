﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrzewoKlasyfikacyjne
{
    public static class Extension
    {
        public static DataView StworzPustaTabeleZTymiSamymiKolumnami(this DataView dataView)
        {
            DataView result = new DataView();
            result.Table = new DataTable("Table");

            foreach (DataColumn column in dataView.Table.Columns)
            {
                result.Table.Columns.Add(column.ColumnName);
            }

            return result;
        }

        /// <summary>
        /// Tworzy listę wszystkich elementów składających się z dzieci danego elementu.
        /// </summary>
        /// <param name="podsumowaniaLista"></param>
        /// <returns></returns>
        public static List<PodsumowanieTabeli> PodajListePotomkow(this List<PodsumowanieTabeli> podsumowaniaLista)
        {
            List<PodsumowanieTabeli> potomkowie = new List<PodsumowanieTabeli>();
            foreach (PodsumowanieTabeli podsumowanie in podsumowaniaLista)
            {
                potomkowie.AddRange(podsumowanie.Dzieci);
            }
            return potomkowie;
        }

    }
}
