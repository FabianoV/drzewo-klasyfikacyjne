﻿using GraphLayout;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TreeContainer;

namespace DrzewoKlasyfikacyjne
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        DataView dataView = null;

        public MainWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Przycisk, który odczytuje dane z pliku Excel.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Tree.Clear();

            OpenFileDialog openfile = new OpenFileDialog();
            openfile.DefaultExt = ".xlsx";
            openfile.Filter = "(.xlsx)|*.xlsx";

            var browsefile = openfile.ShowDialog();

            if (browsefile == true)
            {
                Microsoft.Office.Interop.Excel.Application excelApp = new Microsoft.Office.Interop.Excel.Application();
                //Static File From Base Path...........
                //Microsoft.Office.Interop.Excel.Workbook excelBook = excelApp.Workbooks.Open(AppDomain.CurrentDomain.BaseDirectory + "TestExcel.xlsx", 0, true, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                //Dynamic File Using Uploader...........
                Microsoft.Office.Interop.Excel.Workbook excelBook = excelApp.Workbooks.Open(openfile.FileName, 0, true, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                Microsoft.Office.Interop.Excel.Worksheet excelSheet = (Microsoft.Office.Interop.Excel.Worksheet)excelBook.Worksheets.get_Item(1); ;
                Microsoft.Office.Interop.Excel.Range excelRange = excelSheet.UsedRange;

                string strCellData = "";
                double douCellData;
                int rowCnt = 0;
                int colCnt = 0;

                DataTable dt = new DataTable();
                for (colCnt = 1; colCnt <= excelRange.Columns.Count; colCnt++)
                {
                    string strColumn = "";
                    strColumn = (string)(excelRange.Cells[1, colCnt] as Microsoft.Office.Interop.Excel.Range).Value2;
                    dt.Columns.Add(strColumn, typeof(string));
                }


                for (rowCnt = 2; rowCnt <= excelRange.Rows.Count; rowCnt++)
                {
                    string strData = "";
                    for (colCnt = 1; colCnt <= excelRange.Columns.Count; colCnt++)
                    {
                        try
                        {
                            strCellData = (string)(excelRange.Cells[rowCnt, colCnt] as Microsoft.Office.Interop.Excel.Range).Value2;
                            strData += strCellData + "|";
                        }
                        catch (Exception ex)
                        {
                            douCellData = (excelRange.Cells[rowCnt, colCnt] as Microsoft.Office.Interop.Excel.Range).Value2;
                            strData += douCellData.ToString() + "|";
                        }
                    }
                    strData = strData.Remove(strData.Length - 1, 1);
                    dt.Rows.Add(strData.Split('|'));
                }

                dataGrid.ItemsSource = dt.DefaultView;
                this.dataView = dt.DefaultView;

                excelBook.Close(true, null, null);
                excelApp.Quit();
            }
        }

        /// <summary>
        /// Oblicza cały algorytm.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Oblicz_Click(object sender, RoutedEventArgs e)
        {
            if (dataView == null) return;
            int liczbaPoziomowDrzewaMax = Convert.ToInt32(LiczbaPoziomowDrzewa.Text);
            int obecnyPoziomDrzewa = 0;

            int uczenieValue = Convert.ToInt32(UczenieSlider.Value);
            int walidacjaValue = Convert.ToInt32(WalidacjaSlider.Value);

            //Dzielimy dane na dane uczące i dane walidujące według ustawień aplikacji.
            PodzialDanych podzial = new PodzialDanych(dataView, uczenieValue, walidacjaValue);

            //Budujemy Drzewo klasyfikacyjne
            #region Tworzenie drzewa
            //1 poziom drzewa - korzeń
            PodsumowanieTabeli podsumowanieRoot = new PodsumowanieTabeli(podzial.DaneUczenie);
            podsumowanieRoot.ObliczNajmniejszaEntropie();
            podsumowanieRoot.StworzPodzbiory();
            obecnyPoziomDrzewa++;
            List<PodsumowanieTabeli> poziomLista = new List<PodsumowanieTabeli>() { podsumowanieRoot };

            //2 poziom drzewa i kolejne - potomkowie, dzieci
            while (obecnyPoziomDrzewa < liczbaPoziomowDrzewaMax)
            {
                List<PodsumowanieTabeli> potomkowie = poziomLista.PodajListePotomkow();
                potomkowie.ForEach(podsumowanieDziecko => podsumowanieDziecko.ObliczNajmniejszaEntropie());
                potomkowie.ForEach(podsumowanieDziecko => podsumowanieDziecko.StworzPodzbiory());
                obecnyPoziomDrzewa++;
                poziomLista = potomkowie;
            }
            #endregion tworzenie Drzewa

            //oblicza i ustawia poziomy drzewa w każdym elemencie.
            podsumowanieRoot.UstawPoziomDrzewa();

            //Walidacja drzewa Klasyfikacyjnego
            foreach (DataRow row in podzial.DaneWalidacja.Table.Rows)
            {
                podsumowanieRoot.Waliduj(row);
            }

            RysujDrzewo(podsumowanieRoot);
        }

        private void RysujDrzewo(PodsumowanieTabeli podsumowanieRoot)
        {
            Tree.Clear();
            podsumowanieRoot.CreateControl(Tree, null);

            //TreeNode root = Tree.AddRoot("Root");
            //Tree.AddNode("Child", root);

            //foreach (PodsumowanieTabeli dziecko in podsumowaniaZPoziomu.Dzieci)
            //{
            //    TreeNode node = new TreeNode();
            //    node.Content = dziecko.PoziomDrzewa;
            //    Tree.AddNode(node, root);
            //}
        }

        private void WalidacjaSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (UczenieSlider != null)
            {
                UczenieSlider.Value = 100 - WalidacjaSlider.Value;
            }
        }

        private void UczenieSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (WalidacjaSlider != null)
            {
                WalidacjaSlider.Value = 100 - UczenieSlider.Value;
            }
        }

    }

    public class PodsumowanieTabeli
    {
        //Fields
        int id;
        int poziomDrzewa;
        bool czyLisc;
        List<PodsumowanieKolumny> kolumny;
        PodsumowanieKolumny kolumnaNajnizszaEntropia;
        string wartoscPodzialu;
        DataView zbiorUczacy;
        DataView zbiorWalidacyjny;
        TreeNode nodeRef;
        List<string> uzyteKolumny;
        DataView zbiorZatwierdzony;
        DataView zbiorOdrzucony;

        //Properties
        //Dane
        /// <summary>
        /// Liczba wsyztkich rekordów w kolumnie.
        /// </summary>
        public int LiczbaRekordow { get { return ZbiorUczacy.Table.Rows.Count; } }
        /// <summary>
        /// Zbiór kolumn w danej tabeli.
        /// </summary>
        public List<PodsumowanieKolumny> Kolumny { get { return kolumny; } }
        /// <summary>
        /// Kolumna wybrana do podziału cech.
        /// </summary>
        public PodsumowanieKolumny KolumnaZNajnizszaEntropia { get { return kolumnaNajnizszaEntropia; } }
        /// <summary>
        /// Wartość drzewa dzięki której drzewo zostało podzielone na 2 części. 
        /// Np. w kolumnie kształt są wartości: okrągły i kwadratowy.
        /// Ta kolumna mówi czy do tego liścia klasyfikowane są np wartości kwadratowe.
        /// </summary>
        public string WartoscPodzialu { get { return wartoscPodzialu; } set { wartoscPodzialu = value; } }
        /// <summary>
        /// Wszystkie nazwy kolumn, które zostały już użyte do klasyfikacji u przodka.
        /// </summary>
        public List<string> UzyteKolumny { get { return uzyteKolumny; } }
        /// <summary>
        /// Tabela z danymi - przechowuje wszystkie rekordy danych i wykorzystuje te rekordy do budowy drzewa.
        /// </summary>
        public DataView ZbiorUczacy { get { return zbiorUczacy; } set { zbiorUczacy = value; } }
        /// <summary>
        /// Czy dany węzeł drzewa jest liściem.
        /// </summary>
        public bool CzyLisc { get { return czyLisc; } }

        //Ocena klasyfikacji
        /// <summary>
        /// Tabela z danymi - przechowuje wszystkie rekordy użyte do walidacji wcześniej zbudowanego drzewa.
        /// </summary>
        public DataView ZbiorWalidacyjny { get { return zbiorWalidacyjny; } }
        /// <summary>
        /// Zbior tworzony dla wszystkich węzłów, który posiada wszystkie rekordy poddane walidacji, które 
        /// </summary>
        public DataView ZbiorZatwierdzony { get { return zbiorZatwierdzony; } }
        public DataView ZbiorOdrzucony { get { return zbiorOdrzucony; } }

        #region doc
        //               |        Przewidziana klasa
        // --------------+----------+----------+----------+
        //               |          |    0     |     1    |
        //  Rzeczywista  +----------+----------+----------+
        //  Klasa        |    0     |  A(TP)   |   B(FN)  |
        //               +----------+----------+----------+
        //               |    1     |  C(FP)   |   D(TN)  |
        // --------------+----------+----------+----------+
        //  A: TP (true positive) – prawidłowa klasyfikacja do klasy 1
        //  B: FN (false negative) – nieprawidłowa klasyfikacja do klasy 0
        //  C: FP (false positive) – nieprawidłowa klasyfikacja do klasy 1
        //  D: TN (true negative) – prawidłowa klasyfikacja do klasy 0
        #endregion doc
        ///// <summary>
        ///// True - Positive.
        ///// </summary>
        //public int TP { get; set; }
        ///// <summary>
        ///// True - Negative.
        ///// </summary>
        //public int TN { get; set; }
        ///// <summary>
        ///// False - Positive.
        ///// </summary>
        //public int FP { get; set; }
        ///// <summary>
        ///// False - Negative.
        ///// </summary>
        //public int FN { get; set; }

        //Struktura drzewa
        /// <summary>
        /// Mówi na którym poziomie drzewa dany węzeł się znajduje.
        /// </summary>
        public int PoziomDrzewa { get { return poziomDrzewa; } }
        /// <summary>
        /// Każde dziecko będzie miało inny podzbiór danych i będzie nowym obiektem które będzie wynikało z tego obiektu.
        /// </summary>
        public List<PodsumowanieTabeli> Dzieci { get; set; }
        /// <summary>
        /// Element ojciec dla danego elementu.
        /// </summary>
        public PodsumowanieTabeli Ojciec { get; set; }
        /// <summary>
        /// referencja do obiektu gałezi drzewa.
        /// </summary>
        //public TreeNode NodeRef { get { return NodeRef; } }

        //Constructor
        /// <summary>
        /// Konstruktor dla elemntu root w drzewie.
        /// </summary>
        /// <param name="zbiorUczacy">Tabela z danymi.</param>
        public PodsumowanieTabeli(DataView zbiorUczacy)
        {
            this.id = IdGenerator.GetNextId();
            this.czyLisc = false;
            this.kolumny = new List<PodsumowanieKolumny>();
            this.uzyteKolumny = new List<string>();
            this.Dzieci = new List<PodsumowanieTabeli>();
            this.zbiorUczacy = zbiorUczacy;
            this.zbiorZatwierdzony = new DataView() { Table = zbiorUczacy.Table.Clone(), };
            this.zbiorZatwierdzony.Table.Rows.Clear();

            //Policz ilość wystapień wszystkich unikalnych wartości.
            for (int i = 0; i < this.ZbiorUczacy.Table.Columns.Count; i++)
            {
                string nazwaObecnejKolumny = this.ZbiorUczacy.Table.Columns[i].ColumnName;
                PodsumowanieKolumny podsumowanieKolumny = new PodsumowanieKolumny(nazwaObecnejKolumny, this);

                //Pomiń kolumnę Lp. ponieważ to nie jest cecha.
                if (podsumowanieKolumny.NazwaKolumny != "Lp")
                    this.Kolumny.Add(podsumowanieKolumny);
            }
        }
        /// <summary>
        /// Konstruktor dla elementów child.
        /// </summary>
        /// <param name="tabela">Tabela z danymi.</param>
        /// <param name="ojciec">Obiekt ojciec w drzewie.</param>
        public PodsumowanieTabeli(DataView tabela, PodsumowanieTabeli ojciec)
            : this(tabela)
        {
            this.uzyteKolumny = ojciec.UzyteKolumny;
        }

        //Methods
        /// <summary>
        /// Znajdź kolumne z najmniejsza Entropią.
        /// </summary>
        public void ObliczNajmniejszaEntropie()
        {
            this.kolumnaNajnizszaEntropia = this.Kolumny[0];
            for (int i = 1; i < this.Kolumny.Count; i++)
            {
                //Jeśli dana kolumna była już użyta do klasyfikacji to ją pomijamy.
                if (uzyteKolumny.Contains(this.Kolumny[i].NazwaKolumny))
                    continue;
                
                //jeśli kolumna obecna ma więskzą entropię niż kolumna następna.
                if (this.KolumnaZNajnizszaEntropia.Entropia > this.Kolumny[i].Entropia)
                    this.kolumnaNajnizszaEntropia = this.Kolumny[i];
            }

            //Dodajemy obecnie wybraną kolumnę do użytych kolumn.
            if (this.KolumnaZNajnizszaEntropia != null)
                this.uzyteKolumny.Add(this.KolumnaZNajnizszaEntropia.NazwaKolumny);
        }
        /// <summary>
        /// Stwórz podział/dzieci na podstawie kolumny/kryterium z najniższą entropią.
        /// </summary>
        public void StworzPodzbiory()
        {
            PodsumowanieTabeli ojciec = this;
            foreach (string wartosc in ojciec.KolumnaZNajnizszaEntropia.LiczbaWystapien.Keys)
            {
                //Weź wszystkie wartości od ojca i przepisz wartość dla dziecka.
                DataView dataViewDziecko = ojciec.ZbiorUczacy.StworzPustaTabeleZTymiSamymiKolumnami();
                foreach (DataRow row in ojciec.ZbiorUczacy.Table.Rows)
                {
                    string obecnaWartosc = row[ojciec.KolumnaZNajnizszaEntropia.NazwaKolumny].ToString();
                    if (obecnaWartosc == wartosc)
                    {
                        dataViewDziecko.Table.ImportRow(row);
                    }
                }

                //Stwórz dziecko i podaj mu tabelę.
                PodsumowanieTabeli dziecko = new PodsumowanieTabeli(dataViewDziecko, ojciec) { WartoscPodzialu = wartosc, };
                dziecko.Ojciec = ojciec;
                ojciec.Dzieci.Add(dziecko);
            }
        }
        /// <summary>
        /// Metoda ustawia wszystkie poziomy drzewa, zaczynając od korzenia. Rekurencja.
        /// UWAGA!!! Metoda powinna byc wywołana tylko z korzenia drzewa.
        /// 
        /// Metoda ustawia także pole czyLisc. Pole które wskazuje czy dany węzeł jest liściem dla drzewa.
        /// </summary>
        public void UstawPoziomDrzewa()
        {
            this.poziomDrzewa = 0;
            foreach (PodsumowanieTabeli dziecko in this.Dzieci)
            {
                dziecko.UstawPoziomDrzewa(this.poziomDrzewa + 1);
            }

            if (this.Dzieci.Count == 0)
            {
                this.czyLisc = true;
            }
        }
        /// <summary>
        /// Ustawia wszystkie poziomy drzewa zaczynając od danego poziomu. Rekurencja.
        /// 
        /// Metoda ustawia także pole czyLisc. Pole które wskazuje czy dany węzeł jest liściem dla drzewa.
        /// </summary>
        private void UstawPoziomDrzewa(int poziom)
        {
            this.poziomDrzewa = poziom;
            foreach (PodsumowanieTabeli dziecko in this.Dzieci)
            {
                dziecko.UstawPoziomDrzewa(this.poziomDrzewa + 1);
            }

            if (this.Dzieci.Count == 0)
            {
                this.czyLisc = true;
            }
        }
        /// <summary>
        /// Metoda sprawdza jak drzewo klasyfikacyjne zostało zbudowane.
        /// </summary>
        /// <param name="zbiorWalidacyjny">Tabela z danymi do walidacji.</param>
        public void Waliduj(DataRow row)
        {
            this.zbiorZatwierdzony.Table.ImportRow(row);

            if (this.KolumnaZNajnizszaEntropia == null)
                return;

            //Wartość kolumny dla której wykonywana klasyfikacja w tym węźle drzewa.
            string kolumnaWartosc = (string)row[this.KolumnaZNajnizszaEntropia.NazwaKolumny];
            //Znajdź dziecko dla którego pasuje ta komórka danych.
            PodsumowanieTabeli dzieckoSklasyfikowane = this.Dzieci.FirstOrDefault(dz => dz.WartoscPodzialu == kolumnaWartosc);

            if (dzieckoSklasyfikowane == null)
            {
                return;
            }
            else
            {
                dzieckoSklasyfikowane.Waliduj(row);
            }
        }

        private void Waliduj(PodsumowanieTabeli root, DataRow rekordDoWalidacji)
        {

        }

        /// <summary>
        /// Tworzy graficzną kontrolke - content dla węzła drzewa. 
        /// </summary>
        /// <returns></returns>
        public Wezel CreateControl(TreeContainer.TreeContainer tree, PodsumowanieTabeli ojciec)
        {
            Wezel wezelControl = new Wezel(this) { NazwaWartosciKlasyfikacji = this.WartoscPodzialu, };

            //Tworzenie drzewa:  http://www.codeproject.com/Articles/29518/A-Graph-Tree-Drawing-Control-for-WPF?fid=1526819&fr=26
            if (ojciec == null)
            {
                this.nodeRef = tree.AddRoot(wezelControl);
            }
            else
            {
                this.nodeRef = tree.AddNode(wezelControl, ojciec.nodeRef);
            }

            foreach (PodsumowanieTabeli dziecko in this.Dzieci)
            {
                dziecko.CreateControl(tree, this);
            }

            return wezelControl;
        }

        public override string ToString()
        {
            return string.Format("Element Nazwa: {0}, Kryterium: {1}, czy lisć: {2}", this.kolumnaNajnizszaEntropia.NazwaKolumny, this.wartoscPodzialu, this.czyLisc);
        }
    }

    public class PodsumowanieKolumny
    {
        //Fields
        string nazwaKolumny;
        PodsumowanieTabeli tabela;
        Dictionary<string, int> liczbaWystapien;

        //Properties
        /// <summary>
        /// Nazwa danej kolumny.
        /// </summary>
        public string NazwaKolumny { get { return nazwaKolumny; } }
        /// <summary>
        /// Liczba wsyztkich rekordów w kolumnie.
        /// </summary>
        public int LiczbaRekordow { get { return Tabela.LiczbaRekordow; } }
        /// <summary>
        /// Tabela która zawiera daną kolumnę.
        /// </summary>
        public PodsumowanieTabeli Tabela { get { return tabela; } }
        /// <summary>
        /// Klucz: unikalna wartość,
        /// Wartosc: liczba wystąpień wartości klucza w kolumnie.
        /// </summary>
        public Dictionary<string, int> LiczbaWystapien { get { return liczbaWystapien; } }

        /// <summary>
        /// Entropia obliczona na podstawie wzoru: 
        /// </summary>
        public double Entropia
        {
            get
            {
                double entropia = 0;
                foreach (KeyValuePair<string, int> wartosc in LiczbaWystapien)
                {
                    double c = (double)wartosc.Value / (double)LiczbaRekordow;
                    entropia += (-c * Math.Log(c, 2));
                }

                return entropia;
            }
        }

        //Constructor
        /// <summary>
        /// Tworzy podsumowanie dla danej kolumny.
        /// </summary>
        /// <param name="nazwaKolumny">Nazwa kolumny dla której ten obiekt przechowuje dane.</param>
        /// <param name="tabela">Obiekt podsumowania całej tabeli, czyli obiekt tabeli do której należy kolumna.</param>
        /// <param name="dane">Referencja do tabeli z danymi dla tej kolumny - potrzebnymi do uzupełnienia danych.</param>
        public PodsumowanieKolumny(string nazwaKolumny, PodsumowanieTabeli tabela)
        {
            this.nazwaKolumny = nazwaKolumny;
            this.liczbaWystapien = new Dictionary<string, int>();
            this.tabela = tabela;

            //Policz liczbe wystąpień wszystkich wartości w tabeli i zapisz w słowniku LiczbaWystapien.
            foreach (DataRow row in this.tabela.ZbiorUczacy.Table.Rows)
            {
                string wartosc = row[NazwaKolumny].ToString();
                if (LiczbaWystapien.ContainsKey(wartosc))
                {
                    LiczbaWystapien[wartosc]++;
                }
                else
                {
                    LiczbaWystapien.Add(wartosc, 1);
                }
            }
        }

        public override string ToString()
        {
            return string.Format("{0}, Liczba różnych wartości: {1}", NazwaKolumny, LiczbaWystapien.Count);
        }
    }
}
