﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrzewoKlasyfikacyjne
{
    public static class IdGenerator
    {
        private static int lastId = 0;

        public static int GetNextId()
        {
            return lastId++;
        }
    }
}
