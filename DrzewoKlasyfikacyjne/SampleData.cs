﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrzewoKlasyfikacyjne
{
    public class SampleData
    {
        public enum Kolory { Zielony, Zolty, Czerwony,  }
        public enum Kontury { Przerywany, Ciagly }
        public enum Kropka { Tak, Nie }
        public enum Klasa { Trojkat, Kwadrat }

        public DataSet SampleDataSet { get; set; }

        public SampleData()
        {
            this.SampleDataSet = GetSampleData();
        }

        public DataSet GetSampleData()
        {
            DataSet dataSet = new DataSet();
            DataTable table = new DataTable();
            dataSet.Tables.Add(table);

            DataColumn ColorDataColumn = new DataColumn("Kolor");
            table.Columns.Add(ColorDataColumn);

            DataColumn KonturDataColumn = new DataColumn("Kontur");
            table.Columns.Add(KonturDataColumn);

            DataColumn KropkaDataColumn = new DataColumn("Kropka");
            table.Columns.Add(KropkaDataColumn);

            DataColumn KsztaltDataColumn = new DataColumn("Kształt");
            table.Columns.Add(KsztaltDataColumn);

            table.Rows.Add(Kolory.Zielony, Kontury.Przerywany, Kropka.Nie, Klasa.Trojkat);
            table.Rows.Add(Kolory.Zielony, Kontury.Przerywany, Kropka.Tak, Klasa.Trojkat);
            table.Rows.Add(Kolory.Zolty, Kontury.Przerywany, Kropka.Nie, Klasa.Kwadrat);
            table.Rows.Add(Kolory.Czerwony, Kontury.Przerywany, Kropka.Nie, Klasa.Kwadrat);
            table.Rows.Add(Kolory.Czerwony, Kontury.Ciagly, Kropka.Nie, Klasa.Kwadrat);
            table.Rows.Add(Kolory.Czerwony, Kontury.Ciagly, Kropka.Tak, Klasa.Trojkat);
            table.Rows.Add(Kolory.Zielony, Kontury.Ciagly, Kropka.Nie, Klasa.Kwadrat);
            table.Rows.Add(Kolory.Zielony, Kontury.Przerywany, Kropka.Nie, Klasa.Trojkat);
            table.Rows.Add(Kolory.Zolty, Kontury.Ciagly, Kropka.Tak, Klasa.Kwadrat);
            table.Rows.Add(Kolory.Czerwony, Kontury.Ciagly, Kropka.Nie, Klasa.Kwadrat);
            table.Rows.Add(Kolory.Zielony, Kontury.Ciagly, Kropka.Tak, Klasa.Kwadrat);
            table.Rows.Add(Kolory.Zolty, Kontury.Przerywany, Kropka.Tak, Klasa.Kwadrat);
            table.Rows.Add(Kolory.Zolty, Kontury.Ciagly, Kropka.Nie, Klasa.Kwadrat);
            table.Rows.Add(Kolory.Czerwony, Kontury.Przerywany, Kropka.Tak, Klasa.Trojkat);

            return dataSet;
        }
    }
}
