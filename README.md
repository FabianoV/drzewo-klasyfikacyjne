# Decision Tree Example

The WPF application enables the creation of a classification tree based on input data using the entropy calculation method. It analyzes datasets, identifies key attributes, and generates a decision tree structure to support classification and decision-making processes. The intuitive interface allows users to visualize results, explore nodes, and assess model quality. The application is ideal for data analysis, machine learning, and solving classification problems.

For more information about decision trees, you can visit [this Wikipedia page](https://en.wikipedia.org/wiki/Decision_tree_learning).

## How to run application

1) Download repository

2) Build and run 'DrzewoKlasyfikacyjne' project

3) Load data from 'PrzykladoweDane/Dane.xlsx' file

4) Click calculate button

---

# Application preview
![Preview](Content/scr1.png)

---

## Author

Fabian Oleksiuk

License: MIT
